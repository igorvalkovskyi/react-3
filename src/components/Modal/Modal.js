
import React from "react";
import { IoMdClose } from "react-icons/io";
import "./Modal.css";

export const Modal = ({ isOpen, onClose, children }) => {
  return (
    <>
      {isOpen && (
        <div className="modal">
          <div className="modal-wrapper">
            <div className="modal-content">
              <IoMdClose
                className="modal-close-button"
                onClick={() => onClose()}
              />
              {children}
            </div>
          </div>
        </div>
      )}
    </>
  );
};

