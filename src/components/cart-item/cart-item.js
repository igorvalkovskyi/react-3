import React from "react";
import "./cart-item.css";

export const CartItem = ({name,price,id}) => {
    return (
        <div className="cart-item"> 
        <span>{ name }</span>
        <div className="cart-item__price">
<span>{ price } грн</span>
        </div>
        </div>
    ) 
}