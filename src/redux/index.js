import { configureStore } from "@reduxjs/toolkit";
import cartReducer from "./cart/reducer";
import productsReducer from "./products/reducer";
import favoriteReducer from "./favorite/reducer";

export const store = configureStore({
    reducer: {
        cart: cartReducer,
        products: productsReducer,
        favorite: favoriteReducer,
    },
});