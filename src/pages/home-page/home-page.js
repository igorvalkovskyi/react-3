import React from "react";
import './home-page.css';
 import ProductsItem from "../../components/product-item/products-item";
import { useContext } from "react";
import { ThemeContext } from "../../context/theme-context";


export const HomePage = ({products}) => {

    const theme = useContext(ThemeContext);

return (
    <div className="home-page">
        <div style={{ display: theme.display}}  className="products">
<ProductsItem products={products}/>
        </div>
  
    </div>
)
}
