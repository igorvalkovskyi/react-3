import { createContext } from "react";

export const themes = {
    table: {
      display: "grid",
      
    },
    products:{
        display: "flex",  
    },
};

export const ThemeContext = createContext(themes.products);