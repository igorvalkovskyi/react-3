import React from "react";
import './order-page.css';
import { Button } from "../../components/button";
import { Formik, Form } from "formik";
import * as Yup from "yup";
// import { object } from "prop-types";


export const OrderPage = () => {

const OrderSchema = Yup.object().shape({
    firstName: Yup.string()
    .min(2, 'Занадто коротко!')
    .max(50, 'Занадто довго!')
    .required("Обов'язково до введення"),
    lastName: Yup.string()
    .min(2, 'Занадто коротко!')
    .max(50, 'Занадто довго!')
    .required("Обов'язково до введення"),
    age: Yup.string()
    .min(2, 'Занадто коротко!')
    .max(50, 'Занадто довго!')
    .required("Обов'язково до введення"),
    address: Yup.string()
    .min(5, 'Занадто коротко!')
    .max(50, 'Занадто довго!')
    .matches(/^[а-яА-Я0-9/-/.]+$/, 'Заборонені символи')
    .required("Обов'язково до введення"),
    phone: Yup.string()
    .min(2, 'Занадто коротко!')
    .max(50, 'Занадто довго!')
    .required("Обов'язково до введення"),
})

const handleSubmit = (values)  => {
console.log(values)
}

    return(
        <section className="order-section">
        <Formik
        
        initialValues={{
            firstName: "" ,
            lastName: "" ,
            age: "",
            address: "",
            phone: ""
        }}
        
        validationSchema={OrderSchema}
        onSubmit={handleSubmit}
        >
  
  {
    ( {values, errors, touched, handleChange}) => (
        <Form className="order-form">
            <div className="order-h">
            <h1 className="order-h1">Оформлення замовлення</h1>
            <h3 className="order-h3">Ваші контактні дані</h3>
            </div>
            <div className="order__inputs">
            <div className="order__field">
<input placeholder="Ім'я"  className="order__input" type="text" name="firstName" onChange={handleChange} value={values.firstName}></input>
<p className="order__error">{errors.firstName && touched.firstName && errors.firstName}</p>
</div>
<div className="order__field">
<input placeholder="Призвіще" className="order__input" type="text" name="lastName" onChange={handleChange} value={values.lastName}></input>
<p className="order__error">{errors.lastName && touched.lastName && errors.lastName}</p>
</div>
<div className="order__field">
<input placeholder="Вік" className="order__input" type="text" name="age" onChange={handleChange} value={values.age}></input>
<p className="order__error">{errors.age && touched.age && errors.age}</p>
</div>
<div className="order__field">
<input placeholder="Адреса" className="order__input" type="text" name="address" onChange={handleChange} value={values.address}></input>
<p className="order__error">{errors.address && touched.address && errors.address}</p>
</div>
<div className="order__field">
<input placeholder="Телефон" className="order__input" type="text" name="phone" onChange={handleChange} value={values.phone}></input>
<p className="order__error">{errors.phone && touched.phone && errors.phone}</p>
</div>
<Button className="order-btn" type="submit" size="s">
            Замовлення підтверджую
          </Button>
          </div>
          
        </Form>
    )
  }
        </Formik>
        </section>
    )
}