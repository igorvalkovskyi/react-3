import React from "react";
import "./items-in-favorite.css";

export const ItemsInFavorite = ({
    number = 0
}) => {
    return number > 0 ? (
        <div className="items-in-favorite"> 
        { number }
        </div>
    ) : null
}