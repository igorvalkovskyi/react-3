import { createSlice } from "@reduxjs/toolkit";



const productsSlice = createSlice({
    name: 'products',
    initialState: {
сurrentProduct: null
},
reducers: {
    setCurrentProduct: (state, action) => {
       state.сurrentProduct = action.payload;
    }
}
    

});

export const { setCurrentProduct } = productsSlice.actions;
export default productsSlice.reducer;