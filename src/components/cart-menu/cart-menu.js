import React from "react";
import { Button } from "../button/button";
import { CartItem } from "../cart-item";
import { calcTotalPrice } from "../utils";
import "./cart-menu.css";
import { Link } from "react-router-dom";


export const CartMenu = ({ items, onClick }) => {
  return (
    <div className="cart-menu">
      <div className="cart-menu__product-list">
        {items.length > 0 ? items.map((item) => (
          <CartItem
          key={item.name}
          price={item.price}
          name={item.name}
          id={item.id}
          />
        )) : "Корзина пуста"}
      </div>
      {items.length > 0 ? (
        <div className="cart-menu__arrange">
          <div className="cart-menu__total-price">
            <span>Разом</span>
            <span>{calcTotalPrice(items)} грн</span>
          </div>
          <Link to="/order"><Button type="secondary" size="s" onClick={onClick}>
            Оформити замовлення
          </Button>
          </Link>
        </div>
      ) : null}
    </div>
  );
};
