import React from "react";
// import {ProductBuy} from "../product-buy";
// import Product from '../../components/product/product';
import './product-page.css';
import { useSelector } from "react-redux";


export const ProductPage = () => {
    const product = useSelector(state => state.products.currentProduct);
if (!product) return null

return (
    <div className="product-page">
       
       <h1 className="product-page__name">{product.name}</h1>
       <div className="product-page__content">
        <div className="product-page__left">
{/* <Product image={product.image}/> */}
        </div>
        <div className="product-page__right">
            
        </div>
        <div className="product-page__product-buy">
        {/* <ProductBuy item={{image,name,article,price}}/> */}
        </div>
       </div>
    </div>
)
}