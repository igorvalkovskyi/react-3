import React, { useState } from "react";
import "./product-buy.css";
import { useDispatch, useSelector } from "react-redux";
import { Button } from "../button/button";
import { Modal } from "../Modal";
import { deleteItemFromCart, setItemInCart } from "../../redux/cart/reducer";


export const ProductBuy = ({ item }) => {

const dispatch = useDispatch();
const itemsCart = useSelector(state => state.cart.itemsInCart)
const isItemInCart = itemsCart.some(itemdelete =>  { console.log(itemdelete.id,item.id) 
  return itemdelete.id === item.id});

const handleClick = (e) => {

e.stopPropagation();
if ( isItemInCart ) {
  dispatch(deleteItemFromCart(item.id));
}else{
  dispatch(setItemInCart(item));
}
}



  const [modalInfoIsOpen, setModalInfoOpen] = useState(false);
  return (
    <div className="product-buy">
      <span className="product-buy-price">{item.price} грн.</span>
      <Button type={isItemInCart ? "incart" : "primary"} onClick={(e) => { e.stopPropagation(); setModalInfoOpen(true)}}>

      {isItemInCart ? 'Прибрати з корзини' :  'В корзину'}
      </Button>
      <Modal isOpen={modalInfoIsOpen} 
             onClose={(e) => {e.stopPropagation(); setModalInfoOpen(false)}} >
        <h2 className="modal-h2">{isItemInCart ? 'Прибрати з корзини' :  'Додати до корзини?'}</h2>


        <button className="modal-btn" onClick={ (e) => {handleClick(e);setModalInfoOpen(false) } } >Так</button>
      </Modal>
    </div>
  );
};
