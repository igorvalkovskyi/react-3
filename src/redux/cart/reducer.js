import { createSlice } from "@reduxjs/toolkit";



const initialItems = localStorage.getItem('cart');
const products = initialItems ? JSON.parse(localStorage.getItem('cart')) : [];
 

const cartSlice = createSlice({




    name: 'cart',
    initialState: {
itemsInCart: products,
allProducts: [],
},
reducers: {
    setItemInCart: (state, action) => {
       state.itemsInCart.push(action.payload) 
    },
    deleteItemFromCart: (state, action) => {
        state.itemsInCart = state.itemsInCart.filter(item => item.id !== action.payload)
    },
setProducts: (state, action) => {
    state.allProducts = action.payload 
}
}    

});

export const { setItemInCart, deleteItemFromCart, setProducts } = cartSlice.actions;
export const fetchProducts = () => async dispatch => { 
        fetch("products.json")
        .then(response => response.json())
        .then(data => dispatch(setProducts(data)));  


}
export default cartSlice.reducer;