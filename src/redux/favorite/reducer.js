import { createSlice } from "@reduxjs/toolkit";



const initialItems = localStorage.getItem('favorite');
const products = initialItems ? JSON.parse(localStorage.getItem('favorite')) : [];
 

const favoriteSlice = createSlice({


    name: 'favorite',
    initialState: {
        itemsInFavorite: products
},
reducers: {
    toggleFavorite: (state, action) => {
      const filterFavorite = state.itemsInFavorite.filter(item => item.id !== action.payload.id); 
      const initialLength = state.itemsInFavorite.length;
      const filterLength = filterFavorite.length;
      if (initialLength===filterLength){
 state.itemsInFavorite.push(action.payload)
      }else{
        console.log(deleteItemFromFavorite);
        state.itemsInFavorite = filterFavorite;
      }
    },
    deleteItemFromFavorite: (state, action) => {
        state.itemsInFavorite = state.itemsInFavorite.filter(item => item.id !== action.payload.id)
    }
}
    

});

export const { toggleFavorite, deleteItemFromFavorite } = favoriteSlice.actions;
export default favoriteSlice.reducer;