import React from 'react';
import {render, fireEvent} from '@testing-library/react';
import {Modal} from '../components/Modal/Modal';
import '@testing-library/jest-dom/extend-expect';


test('Modal renders correctly', () => {

    const isItemInCart = true;
  const { getByText } = render(<Modal isOpen={true} > <h2 className="modal-h2">{isItemInCart ? 'Прибрати з корзини' :  'Додати до корзини?'}</h2></Modal>);
  const modalContent = getByText('Прибрати з корзини');
  expect(modalContent).toBeInTheDocument();
});

test('Modal closes on close button click', () => {
  const onCloseMock = jest.fn();
  const { getByText } = render(<Modal isOpen={true} content="Modal Content" onClose={onCloseMock}> <button className="modal-btn" onClick={ (e) => {onCloseMock() } } >Так</button></Modal>);
  const closeButton = getByText('Так');

  fireEvent.click(closeButton);

  expect(onCloseMock).toHaveBeenCalled();
});

test("renders modal", () => {

    const { asFragment } = render(<Modal/>);

    expect(asFragment(<Modal/>)).toMatchSnapshot();
});