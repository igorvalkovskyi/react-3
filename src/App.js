import {
  BrowserRouter as Router,
Route,
Routes

} from "react-router-dom"; 
import { HomePage } from "./pages/home-page";
import { ProductPage } from "./pages/product-page";
import { FavoritePage } from "./pages/favorite-page";
import { CartPage } from "./pages/cart-page";
import { OrderPage } from "./pages/order-page";
import { Header } from "./components/header";
import { useSelector } from "react-redux/es/hooks/useSelector";
import { useState, useEffect} from 'react';
import { fetchProducts } from "./redux/cart/reducer";
import { useDispatch } from "react-redux";
import { ThemeContext, themes } from "./context/theme-context";

function App() {
  const [currentTheme, setCurrentTheme] = useState(themes.products);
  const [products,setProducts] = useState([])
  const items = useSelector(state => state.cart.itemsInCart);
  const favoriteItems = useSelector(state => state.favorite.itemsInFavorite);
  const dispatch = useDispatch();

const toggleTheme = () => {
  setCurrentTheme((prevCurrentTheme) => prevCurrentTheme === themes.products ? themes.table : themes.products
   );
};


  useEffect(() => { 
    dispatch (fetchProducts())
      fetch("products.json")
      .then(response => response.json())
      .then(data => setProducts(data));  } , []
      )

    useEffect(() => {
      localStorage.setItem('cart', JSON.stringify(items))
    }, [items]
    )
    useEffect(() => {
      localStorage.setItem('favorite', JSON.stringify(favoriteItems))
    }, [favoriteItems]
    )

  return (
    <>
    <Router>
    <div className="App">
      <Header />
      <Routes>

       <Route path="/" element={ <ThemeContext.Provider value={currentTheme}> <HomePage products={products} /><button className="kind" onClick={toggleTheme}>Вид</button></ThemeContext.Provider> } /> 
       
       
        <Route path="/app/:title" element={ <ProductPage /> } />  
        <Route path="/order" element={ <OrderPage /> } />
        <Route path="/favorite" element={ <FavoritePage products={products} /> } />
        <Route path="/cart" element={ <CartPage products={products}/> } />
      </Routes>
    </div>
    </Router>
    </>
  );
}

export default App;
