import React, { useState } from "react";
import { useSelector } from "react-redux";
import { AiOutlineShoppingCart } from "react-icons/ai";
import { IoMdHeart } from "react-icons/io";
import {CartMenu} from "../cart-menu";
import {ItemsInCart} from "../items-in-cart";
import {ItemsInFavorite} from "../items-in-favorite";
import { calcTotalPrice } from "../utils";
import './cart-block.css';
import { Link } from "react-router-dom";
import { FaAngleLeft } from "react-icons/fa6";

export const CartBlock = () => {

    const [isCartMenuVisible, setIsCartMenuVisible] = useState(false);
const items = useSelector(state => state.cart.itemsInCart);
const itemsFavorite = useSelector(state => state.favorite.itemsInFavorite);
const totalPrice = calcTotalPrice(items);



return (
    <div className="cart-block">
        <Link to="/favorite"><IoMdHeart className="heart-block__icon" /></Link>
        <ItemsInFavorite number={itemsFavorite.length}/>
        <ItemsInCart quantity={items.length}/>
        <Link to = "/cart">
        <AiOutlineShoppingCart className="cart-block__icon" />
        </Link>
  {totalPrice > 0 ? <span className="cart-block__total-price">{totalPrice}</span> : null}
{isCartMenuVisible && <CartMenu items={items}  onClick={() => null}/>}
<button className="info-cart" onClick={() => setIsCartMenuVisible(!isCartMenuVisible)}><FaAngleLeft /></button>
    </div>
    
)
}