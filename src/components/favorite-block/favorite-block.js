import React, { useState } from "react";
import { useSelector } from "react-redux";
import {ItemsInFavorite} from "../items-in-favorite";
import { IoMdHeart } from "react-icons/io";
import { Link } from "react-router-dom";

export const FavoriteBlock = () => {
    const itemsFavorite = useSelector(state => state.favorite.itemsInFavorite);

    return (
        <div className="favorite-block">
        <Link to="/favorite"><IoMdHeart className="heart-block__icon" /></Link>
        <ItemsInFavorite number={itemsFavorite.length}/>
        </div>
    
    )
    }