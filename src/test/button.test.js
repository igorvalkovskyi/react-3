import React from 'react';
import {render, fireEvent} from '@testing-library/react';
import {Button} from '../components/button/button';
import '@testing-library/jest-dom/extend-expect';


test("renders button",() => {
    const { getByText } = render(<Button>Test</Button>);
    const linkElement = getByText(/Test/i);
    expect(linkElement).toBeInTheDocument();
});

test("renders button", () => {

    const { asFragment } = render(<Button/>);

    expect(asFragment(<Button/>)).toMatchSnapshot();
});

test("click button", () =>{

    const func = jest.fn();
const {getByRole} = render(<Button onClick={func}/>)
fireEvent.click(getByRole("button"))
expect(func).toHaveBeenCalledTimes(1);
});