import React from "react";
import { useSelector } from "react-redux";
import ProductsItem from "../../components/product-item/products-item";
export const FavoritePage =  ({products}) => {
    const items = useSelector(state => state.favorite.itemsInFavorite);
    const productsArray = products?.filter(product => { return items.find( (element) => element.id === product.id)})
    return(
        <div className="products">
        <ProductsItem products={productsArray}/>
                </div>
    )

}