
import './products-item.css';
import Product from '../product/product';


const ProductsItem = ({products}) => {

const productsArray = products.map(product => {
    return (
    <Product item = {product}/> 
    )
})
return (
<>
{productsArray}
</>

)
}
export default ProductsItem;
