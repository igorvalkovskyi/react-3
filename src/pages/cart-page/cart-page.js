import React from "react";

import { useSelector } from "react-redux";
import ProductsItem from "../../components/product-item/products-item";
export const CartPage =  ({products}) => {
    const items = useSelector(state => state.cart.itemsInCart);
    const productsArray = products?.filter(product => { return items.find((item) => item.id === product.id )})
    console.log(productsArray);
    return(
        <div className="products">
        <ProductsItem products={productsArray}/>
                </div>
    )

}

