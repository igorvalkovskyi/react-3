import "./product.css";
import {ProductBuy} from "../product-buy";
import { FaRegStar } from "react-icons/fa";
import PropTypes from 'prop-types';
import {useNavigate} from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {setCurrentProduct} from "../../redux/products/reducer";
import { toggleFavorite } from "../../redux/favorite/reducer";

const Product = ({item}) => {

const {image,name,article,price,id} = item;
    const navigate = useNavigate();
    const dispatch = useDispatch();   
        const handleClick = (item) => {
    dispatch(setCurrentProduct(item));
    navigate(`/app/${item.name}`);
        };

        const items = useSelector(state => state.favorite.itemsInFavorite);
        const isFavorite = items.some( (element) => element.id === id);
console.log(isFavorite);

const field = isFavorite ? "#00aea9" : "white";

    return (
    <div className = 'product-item' key={item} onClick={() => handleClick(item)}>

     <img className = 'product-item__img' src = {image} alt = {image} />
     
     <div className = 'product-item__name'>{name}</div>
     <div className = 'product-item__article'>Артикул: {article}</div>
    
     <FaRegStar className="lover-product-icon" style={{fill:field}} onClick={(e) => { e.stopPropagation(); dispatch(toggleFavorite(item))}}/>
     
     <div className = 'product-item__buy'>
     <ProductBuy item={{image,name,article,price,id}} 
     /></div>
         </div>)
}

Product.propTypes = {
    name: PropTypes.string,
    article: PropTypes.string,
    price: PropTypes.number
}


export default Product;